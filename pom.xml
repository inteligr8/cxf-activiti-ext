<project xmlns="http://maven.apache.org/POM/4.0.0"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.activiti</groupId>
		<artifactId>activiti-app-root</artifactId>
		<version>24.1.0</version>
	</parent>
	
	<groupId>com.inteligr8.activiti</groupId>
	<artifactId>cxf-activiti-app-ext</artifactId>
	<version>1.3.SNAPSHOT-aps-v24.1</version>
	<packaging>jar</packaging>
	
	<name>CXF-based JAX-RS &amp; JAX-WS enablement for APS App</name>
	<description>An Alfresco Process Service App extension providing the missing components for JAX-RS-based client libraries.</description>
	<url>https://bitbucket.org/inteligr8/cxf-activiti-ext</url>

	<licenses>
		<license>
			<name>GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007</name>
			<url>https://www.gnu.org/licenses/lgpl-3.0.txt</url>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:https://bitbucket.org/inteligr8/cxf-activiti-ext.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:inteligr8/cxf-activiti-ext.git</developerConnection>
		<url>https://bitbucket.org/inteligr8/cxf-activiti-ext</url>
	</scm>
	<organization>
		<name>Inteligr8</name>
		<url>https://www.inteligr8.com</url>
	</organization>
	<developers>
		<developer>
			<id>brian.long</id>
			<name>Brian Long</name>
			<email>brian@inteligr8.com</email>
			<url>https://twitter.com/brianmlong</url>
		</developer>
	</developers>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.source>11</maven.compiler.source>
		<maven.compiler.target>11</maven.compiler.target>
		
		<license.skip>true</license.skip>
	</properties>
	
	<dependencyManagement>
		<dependencies>
			<!-- APS provided libraries -->
			<dependency>
				<groupId>com.sun.xml.bind</groupId>
				<artifactId>jaxb-impl</artifactId>
				<scope>provided</scope>
			</dependency>
			
			<!-- All APS versions v1.11.0 and later have some incarnation of the jaxws-api library; exclude all -->
			<dependency>
			    <groupId>javax.xml.ws</groupId>
			    <artifactId>jaxws-api</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
			    <groupId>jakarta.xml.ws</groupId>
			    <artifactId>jakarta.xml.ws-api</artifactId>
				<scope>provided</scope>
			</dependency>
			
			<!-- All APS versions v1.11.0 and later have some incarnation of the activation library; exclude all -->
			<dependency>
			    <groupId>javax.annotation</groupId>
			    <artifactId>javax.annotation-api</artifactId>
				<scope>provided</scope>  <!-- v1.11.3 and earlier; exclude in v1.11.4 and later for conflict with jakarta-annotation-api -->
			</dependency>
			<dependency>
			    <groupId>jakarta.annotation</groupId>
			    <artifactId>jakarta.annotation-api</artifactId>
				<scope>provided</scope>  <!-- v1.11.4 and later; exclude in v1.11.3 and earlier for conflict with annotation-api -->
			</dependency>
			
			<!-- All APS versions v1.11.0 and later have some incarnation of the activation library; exclude all -->
			<dependency>
			    <groupId>javax.activation</groupId>
			    <artifactId>activation</artifactId>
				<scope>provided</scope>  <!-- v1.11.0 and earlier; exclude in v1.11.1 and later for conflict with jakarta-activation-api -->
			</dependency>
			<dependency>
			    <groupId>com.sun.activation</groupId>
			    <artifactId>javax.activation</artifactId>
				<scope>provided</scope>  <!-- v1.11.3 and later; exclude in v1.11.2 and earlier for conflict with activation -->
			</dependency>
			<dependency>
			    <groupId>jakarta.activation</groupId>
			    <artifactId>jakarta.activation-api</artifactId>
				<scope>provided</scope>  <!-- v1.11.1 and later; exclude in v1.11.2 and earlier for conflict with activation -->
			</dependency>
			
			<!-- All APS versions v1.11.0 and later have some incarnation of the jaxb-api library; exclude all -->
			<dependency>
				<groupId>javax.xml.bind</groupId>
				<artifactId>jaxb-api</artifactId>
				<scope>provided</scope>  <!-- v1.11.0 and earlier; exclude in v1.11.1 and alter for conflict with jakarta.xml.bind-api -->
			</dependency>
			<dependency>
				<groupId>jakarta.xml.bind</groupId>
				<artifactId>jakarta.xml.bind-api</artifactId>
				<scope>provided</scope>  <!-- v1.11.3 and later; exclude in v1.11.2 and earlier for conflict with jaxb-api -->
			</dependency>
			
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-core</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-databind</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-annotations</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.dataformat</groupId>
				<artifactId>jackson-dataformat-yaml</artifactId>
				<scope>provided</scope>
			</dependency>
			
			<dependency>
				<groupId>joda-time</groupId>
				<artifactId>joda-time</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>org.codehaus.woodstox</groupId>
				<artifactId>stax2-api</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>org.codehaus.woodstox</groupId>
				<artifactId>woodstox-core-asl</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>org.apache.ws.xmlschema</groupId>
				<artifactId>xmlschema-core</artifactId>
				<scope>provided</scope>
			</dependency>
			
			<!-- ******************** -->
			<!-- APS v2.x specific -->

			<!-- APS/Chemistry provided libraries -->
			<dependency>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-rt-transports-http</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-core</artifactId>
				<scope>provided</scope>
			</dependency>
			
			<!-- APS v1.11.3 and later provided libraries -->
			<dependency>
				<groupId>org.jboss.spec.javax.rmi</groupId>
				<artifactId>jboss-rmi-api_1.0_spec</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.woodstox</groupId>
				<artifactId>woodstox-core</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>org.apache.geronimo.specs</groupId>
				<artifactId>geronimo-jta_1.1_spec</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.sun.xml.messaging.saaj</groupId>
				<artifactId>saaj-impl</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>jakarta.jws</groupId>
				<artifactId>jakarta.jws-api</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>jakarta.xml.soap</groupId>
				<artifactId>jakarta.xml.soap-api</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.sun.activation</groupId>
				<artifactId>jakarta.activation</artifactId>
				<scope>provided</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- Package CXF JAX-RS support -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-client</artifactId>
			<version>${cxf.version}</version>
			<scope>runtime</scope>
		</dependency>
		<!-- Package JackSON JAX-RS support -->
		<dependency>
			<groupId>com.fasterxml.jackson.jakarta.rs</groupId>
			<artifactId>jackson-jakarta-rs-json-provider</artifactId>
			<scope>runtime</scope>
		</dependency>
		<!-- Package JackSON XML support -->
		<dependency>
			<groupId>com.fasterxml.jackson.dataformat</groupId>
			<artifactId>jackson-dataformat-xml</artifactId>
			<scope>runtime</scope>
		</dependency>
		<!-- Package JackSON joda-time support -->
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-joda</artifactId>
			<scope>runtime</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<!-- Several CXF libraries have a bus-extensions.txt file that will overwrite each other during the packaging process -->
			<!-- Only the last one packaged will have its contents in tact -->
			<!-- We need the contents of all of them and that can be done by simply concatenating them -->
			
			<!-- Extract bus-extensions.txt from all packaged (CXF) libraries -->
			<plugin>
				<artifactId>maven-dependency-plugin</artifactId>
				<executions>
					<execution>
						<id>unpack-bus-extensions</id>
						<phase>prepare-package</phase>
						<goals><goal>unpack-dependencies</goal></goals>
						<configuration>
							<includeTypes>jar</includeTypes>
							<includeScope>runtime</includeScope>
							<includes>META-INF/cxf/bus-extensions.txt</includes>
							<useSubDirectoryPerArtifact>true</useSubDirectoryPerArtifact>
							<outputDirectory>${project.build.directory}/bus-extensions</outputDirectory>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<!-- Concatenate the extracted bus-extensions.txt files -->
			<plugin>
				<groupId>com.inteligr8</groupId>
				<artifactId>merge-maven-plugin</artifactId>
				<version>1.0.0</version>
				<executions>
					<execution>
						<id>merge-bus-extensions</id>
						<phase>prepare-package</phase>
						<goals><goal>merge-dirs</goal></goals>
						<configuration>
							<filesets>
								<fileset>
									<directory>${project.build.directory}/bus-extensions</directory>
									<includes>
										<include>*</include>
									</includes>
									<excludes>
										<exclude>META-INF</exclude>
									</excludes>
									<outputDirectory>${project.build.directory}/bus-extensions</outputDirectory>
								</fileset>
							</filesets>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<executions>
					<execution>
						<id>assemble-jar</id>
						<phase>package</phase>
						<goals><goal>single</goal></goals>
						<configuration>
							<descriptorRefs>
								<descriptorRef>jar-with-dependencies</descriptorRef>
							</descriptorRefs>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>truezip-maven-plugin</artifactId>
				<version>1.2</version>
				<executions>
					<execution>
						<id>overwrite-bus-extensions</id>
						<phase>package</phase>
						<goals><goal>copy</goal></goals>
						<configuration>
							<fileset>
								<directory>${project.build.directory}/bus-extensions</directory>
								<includes>
									<include>META-INF/cxf/bus-extensions.txt</include>
								</includes>
								<outputDirectory>${project.build.directory}/${project.build.finalName}-jar-with-dependencies.jar</outputDirectory>
							</fileset>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>2.40</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-aps-ext-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-aps-ext-rad-tile:[1.0.3,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
		</plugins>
	</build>
	
	<profiles>
		<profile>
			<id>wsl-is-slow</id>
			<activation>
				<os>
					<family>windows</family>
				</os>
			</activation>
			<properties>
				<acs-platform.timeout>1200000</acs-platform.timeout>
			</properties>
		</profile>
		<profile>
			<id>ossrh-release</id>
			<properties>
				<maven.deploy.skip>true</maven.deploy.skip>
			</properties>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-gpg-plugin</artifactId>
						<executions>
							<execution>
								<id>sign</id>
								<phase>verify</phase>
								<goals><goal>sign</goal></goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.sonatype.plugins</groupId>
						<artifactId>nexus-staging-maven-plugin</artifactId>
						<version>1.6.13</version>
						<configuration>
							<serverId>ossrh</serverId>
							<nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
							<autoReleaseAfterClose>true</autoReleaseAfterClose>
						</configuration>
						<executions>
							<execution>
								<id>ossrh-deploy</id>
								<phase>deploy</phase>
								<goals><goal>deploy</goal></goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
	
	<repositories>
		<repository>
			<id>activiti-releases</id>
			<url>https://artifacts.alfresco.com/nexus/content/repositories/activiti-enterprise-releases/</url>
		</repository>
	</repositories>
</project>
