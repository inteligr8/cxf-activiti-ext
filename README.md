
# CXF JAX-RS Enablement for Alfresco Process Services

This is an [Alfresco Process Services](https://www.alfresco.com) extension that provides the missing components to support JAX-RS-based clients.  There is no code, automation, or anything fancy provided by this module.  It just includes the necessary libraries.

## Install

You can install this like any JAR extension.  It just needs to be put in the Tomcat classpath.  Be sure to use the specific major/minor version of this built library when installing in your APS App.  For instance, `aps20` should be used with APS v2.0.x.

## Configuration

There is nothing to configure.
